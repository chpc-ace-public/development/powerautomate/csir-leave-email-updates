# CSIR leave email updates

By Bryan Johnston (bjohnston)
_Senior HPC Technologist - Advanced Computer Engineering (ACE) Lab_

---

**Power Automate (cloud)**
If an approved HR email arrives in Outlook (with designated keywords) then the Flow triggers:

* Outlook schedules an **Automated Reply** based on last scheduled leave transaction (currently it will only schedule the most recent HR email's dates, so this may overwrite an existing one)
* Books out the leave days on Outlook calendar
    * Marks you as OUT OF OFFICE for scheduling appointments
* Notifies designated MS Team of leave transaction (_if defined_ - see below for optional Flow)

## Getting started

First you will need to enable **Power Automate** on the Office365 cloud instance for the CSIR (there are two ways to run Power Automate - remotely on the cloud, or locally on your desktop - this Flow runs on the cloud). 

Once it is enabled, you will import the ZIP file and configure the _connectors_ (these _connect_ your personal CSIR accounts to the services updated by the Flow - such as connecting your Outlook365 account to the Power Automate Flow). 

* Download the appropriate Flow from the list of ZIP files available on this repo
    * The Flow that **_includes_** posting to MS Teams [is available here](https://gitlab.com/chpc-ace-public/development/powerautomate/csir-leave-email-updates/-/blob/main/CSIRleaveemailupdateswithTeamsNotification_20220218091001.zip).

    * The Flow that **_does not post to MS Teams_** [is available here](https://gitlab.com/chpc-ace-public/development/powerautomate/csir-leave-email-updates/-/blob/main/CSIRLeaveEmailUpdates(NOTeams)_20220218090813.zip).
* Launch / activate **Power Automate** Flow for your profile [using this link](https://emea.flow.microsoft.com/en-us/?auth_upn=BJohnston%40csir.co.za&utm_source=office&utm_medium=app_launcher&utm_campaign=office_referrals&showFeaturedTemplates=true).
    * _You may need to _sign in_ to your Microsoft account to activate **Power Automate** for the first time._
* Once in your Power Automate profile (online), select [My Flows](https://emea.flow.microsoft.com/manage/environments/Default-2fd3c5d5-ddb2-4ed3-9803-f89675928df4/flows).
* Select the **Import** option.
* Upload the ZIP file.
* Add / enable the **Office365 Outlook connector**
    * (_Optional_) Add / enable the **MS Teams connector**


## Usage
Submit a leave request via the HR portal. Upon the leave request being approved, a response email will land in your **Inbox** (by default, unless you have changed this behaviour). The Flow triggers on emails arriving in your Inbox from the sender hr-ln-ps-app1@csir.co.za and the phrase _"has been approved"_ in the email text body. Once triggered, the start date and end date are calculated and used to schedule the **Automatic Reply** time window and **out of office** calendar event in Outlook.

_(OPTIONAL) A copy of the leave approval email is posted to a _MS Teams_ channel defined during the import of the Flow._

## Support
Reach out to me or the ACE Lab at the CSIR.

## Roadmap
Potentially expanding this to work more effectively with timesheets and alternative customisations as suggested.

## Contributing & Testing
After importing to Power Automate, you can modify it to run internal tests, etc. Feel free to share what you learn / figure out!

## Authors and acknowledgment
Lara Timm (Engineer, ACE Lab).

## License
Knock yourself out!

## Project status
Done and dusted in its preliminary state as of 20220212 (read that as YYYYMMDD).